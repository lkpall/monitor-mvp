from .base import BasePage

from typing import List

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

from time import sleep


class ZapPage(BasePage):
    def __init__(self, driver):
        super(ZapPage, self).__init__(driver)
        self._input_message = 'div[data-testid="conversation-compose-box-input"]'

    def get_publications(self) -> List:
        return super().get_publications()

    def send_msg(self, number: str, msg: str) -> None:
        """
        Envia a mensagem com os anúncios encontrados pelo alerta para o usuário

        Params
        ------
        number: Número para enviar a mensagem
        msg: Mensagem com os anúncios encontrados
        """
        link = f"https://web.whatsapp.com/send?phone=55{number}&text&type=phone_number&app_absent=0"
        self.driver.get(link)
        sleep(2)
        _input = self._wait_to_exist(By.CSS_SELECTOR, self._input_message)
        sleep(3)
        _input.send_keys(msg)
        _input.send_keys(Keys.ENTER)
