from bs4 import BeautifulSoup

from typing import List

from monitor.utils import format_date, format_price

from .base import BasePage


class OlxPage(BasePage):
    def __init__(self, driver):
        super(OlxPage, self).__init__(driver)

        # elements selectors
        self.id_list_publications = 'ad-list'
        self.link = 'a'
        self.title = 'h2'
        self.price = 'span[aria-label^="Preço do item:"]'
        self.location = 'span[aria-label^="Localização:"]'
        self.created_at = 'span[aria-label^="Anúncio publicado em:"]'
        self.img_product = 'img[alt^="Título do anúncio:"]'
        self.pagination = 'div[data-testid="paginationDesktop"] ul li a'
        self.next_page = 'a[data-lurker-detail="next_page"]'

        # obj beautiful soup
        self.soup = BeautifulSoup(driver.page_source, 'html.parser')

    @property
    def _pagination(self):
        return self._query_selector_all(self.pagination)

    def get_publications(self) -> List:
        """
        Percorre pelo DOM HTML da página e extrai os dados dos anúncios

        Returns
        -------
        - Lista de anúncios extraidos da página
        """
        publications = []
        elements_publications = self.soup.find(id=self.id_list_publications)

        if not elements_publications:
            return []

        elements_publications = elements_publications.find_all('li')

        for pub in elements_publications:
            try:
                pub_data = {}
                pub_data['title'] = pub.find(self.title).text
                price = pub.select(self.price)
                price = price[0].text if price else None

                if not price:
                    continue

                pub_data['price'] = format_price(price)
                pub_data['created_at'] = format_date(pub.select(self.created_at)[0].text)
                pub_data['link'] = pub.a['href']
                pub_data['address'] = pub.select(self.location)[0].text
                publications.append(pub_data)
            except AttributeError:
                pass

        return publications
