from abc import ABC, abstractmethod

from typing import List

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait


class BasePage(ABC):
    """
    Abstract class to serve as a base for all other page objects.
    Here is where to put all of the information relevant to all the screens
    """

    def __init__(self, driver):
        self.driver = driver
        self.buttons = {}

    @abstractmethod
    def get_publications(self) -> List:
        """
        Get publications abstract method.
        """
        ...

    def _query_selector(self, selector):
        return self.driver.find_element(By.CSS_SELECTOR, selector)

    def _query_selector_all(self, selector):
        return self.driver.find_elements(By.CSS_SELECTOR, selector)

    def wait(self):
        return WebDriverWait(self.driver, 10)

    def _wait_to_exist(self, type_, locator):
        return self.wait().until(ec.presence_of_element_located((type_, locator)))

    def click_button(self, button):
        self.driver.click(self._query_selector(self.buttons[button]))
