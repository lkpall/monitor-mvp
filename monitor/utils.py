# Arquivo para funções auxiliadoras menores que são utilizadas em grande parte do projeto

from datetime import datetime, timedelta


def format_date(_date: str) -> datetime:
    """
    Formata a data extraida para um formato datetime

    Params
    ------
    _date: Data

    Returns
    -------
    datetime: Data formatada para o tipo de dado datetime
    """
    today = datetime.now().date()
    MONTHS = {'jan': '01', 'fev': '02', 'mar': '03', 'abr': '04',  'mai': '05',  'jun': '06',
              'jul': '07', 'ago': '08', 'set': '09', 'out': '10', 'nov': '11', 'dez': '12'}

    if 'Hoje' in _date:
        _date = _date.replace("Hoje", f"{today}")
    elif 'Ontem' in _date:
        yesterday = today - timedelta(days=1)
        _date = _date.replace("Ontem", f"{yesterday}")
    else:
        month = _date.split()[1][:3]

        if month in MONTHS:
            _date = _date.replace(month, MONTHS[month])

        year = datetime.now().year
        return datetime.strptime(f'{year} ' + _date, "%Y %d %m, %H:%M")

    return datetime.strptime(_date, "%Y-%m-%d, %H:%M")


def format_price(price: str) -> str:
    """
    Retorna o valor do produto

    Params
    ------
    price: Preço do produto no formato 'RS 1000'

    Returns
    -------
    str: Apenas o número equivalente ao valor do produto
    """
    return price.split()[1]
