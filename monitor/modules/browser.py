# Configuração do selenium

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options


def setup_driver(headless=True):
    caps = DesiredCapabilities().CHROME
    caps["pageLoadStrategy"] = "eager"  # interactive
    chrome_options = Options()

    if headless:
        chrome_options.add_argument("--headless")  # open browser in background

    driver = webdriver.Chrome(desired_capabilities=caps, chrome_options=chrome_options)

    return driver
