from email.message import EmailMessage

import ssl
import smtplib
import configparser


def send_email(email_receiver: str, title: str, body: str) -> None:
    # Read config file
    config_obj = configparser.ConfigParser()
    config_obj.read("config.ini")
    email_cf = config_obj['email']

    # Obj email message (headers default)
    em = EmailMessage()
    em["From"] = email_cf["email_sender"]
    em["Subject"] = f"Olá! Seu alerta {title} identificou novos produtos cadastrados!"
    em["To"] = email_receiver
    em.set_content(body)

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(email_cf['host'], email_cf['port'], context=context) as smtp:
        smtp.login(email_cf["email_sender"], email_cf["email_password"])
        smtp.sendmail(email_cf["email_sender"], email_receiver, em.as_string())
