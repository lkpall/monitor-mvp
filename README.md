# Monitor mvp

Esse projeto consiste em uma solução para monitorar anúncios da OLX. 

## Description
Devido a plataforma da OLX não oferecer um sistema próprio de atualizações sobre produtos pesquisados por seus usuários, esse projeto consiste em uma solução alternativa para monitorar anúncios da OLX, dado parâmetros específicos sobre o produto, e notificar ao usuário sobre novos produtos cadastrados baseado nos parâmetros informados.

### O projeto foi definido:
- app/
    - schemas/
    - __init__.py
    - crud.py
    - database.py
    - main.py
    - models.py
- monitor/
    - modules/
        - browser.py
    - notificadores/
        - send_email.py
    - pages/
        - __init__.py
        - base.py
        - olx.py
        - zapzap.py
    - __init__.py
    - helpers.py
    - utils.py

A pasta app/ contém a construção da API responsável pelo CRUD de usuários e alertas, e a definição e configuração do banco de dados. Os alertas eram criados baseado na URL de pesquisa de um produto específico na OLX, quanto mais específico melhor, pois na URL existem parametros que filtravam esses produtos.

Como a plataforma não possui nenhuma API pública para consumo dos dados que são inseridos, foram utilizados várias técnicas de Web Scrapping para conseguir extrair as informações dos produtos e salvar em um banco local. A ideia era usar esses dados salvos no banco para posteriormente gerar insights sobre os produtos mais pesquisados pelos usuários. O Scrapper é executado num intervalo de 10 minutos.

Já na pasta monitor/ é contido a construção do Scrapper de dados da plataforma, notificação por e-mail e por Whatsapp. No desenvolvimento foram utilizados o Beautifoulsoup e Selenium para fazer a extração dos dados no Document Object Model(DOM). Para além da extração dos dados, essas bibliotecas também foram utilizadas como um artíficio para enviar as notificações dos alertas pelo Whatsapp.

## Configuration
É necessário que seja instalado previamente o Selenium Webdriver de preferência para o navegador Chrome.
```
git clone git@gitlab.com:lkpall/monitor-mvp.git
```
Fazer parte da configuração onde é feita a instalação do python e pip

```
virtualenv -p python3 venv
source venv/bin/activate
pip3 install -r requirements.txt
```
### Install sqlite3
```
sudo apt install sqlite3 or
brew install sqlite3
```

### Execute server FastAPI
```
cd monitor-mvp
uvicorn  app.main:app --reload
```

## Monitor
Scraping para as páginas do OLX

## Usage
Antes de aplicação subir e rodar a raspagem de dados e notificações, é aberto um guia no Chrome para que faça login no Whatsapp Web. Após feito o login a aplicação irá raspar os dados para os alertas criados, salvar no banco e notificar os usuários.

## Results
O arquivo publicações.csv é um exemplo dos produtos que foram extraídos pela aplicação.
