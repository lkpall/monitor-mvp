# Arquivo principal pela execução da raspagem de dados (main)

from app.models import Publication

from monitor.pages.olx import OlxPage
from monitor.pages.zapzap import ZapPage
from monitor.modules.browser import setup_driver
from monitor.notificadores.send_email import send_email

from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError


def monitor_publication(id: int, url: str, title: str, notifier: dict, driver_zap: ZapPage, db: Session):
    print("Monitorando...")

    with setup_driver() as driver:
        driver.get(url)
        olx = OlxPage(driver)
        publications = olx.get_publications()
        new_publications = []

        for pub in publications:
            try:
                pub['alert_id'] = id
                pub_obj = Publication(**pub)
                db.add(pub_obj)
                db.commit()
                new_publications.append(
                    f"Titulo: {pub['title']} || preço: {pub['price']} || link: {pub['link']} || Endereço: {pub['address']}"  # NOQA
                )
            except IntegrityError:
                db.rollback()

    if new_publications:
        new_publications = '\n--------------------------------\n'.join(new_publications)

        if notifier.get('email'):
            send_email(notifier['email'], title, new_publications)
        elif notifier.get('tel'):
            driver_zap.send_msg(number=notifier['tel'], msg=new_publications)

        print("Alerta enviado!")

    return
