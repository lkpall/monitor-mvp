"""
Modelos de tabelas criadas no banco
"""
from sqlalchemy import (Boolean, Column, DateTime, ForeignKey, Integer,
                        Numeric, String, UniqueConstraint)
from sqlalchemy.orm import relationship

from .database import Base


class User(Base):
    """Modelo de Usuario"""
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, index=True)
    hashed_password = Column(String)
    is_active = Column(Boolean, default=True)
    tel = Column(String, index=True)
    address = Column(String, index=True)

    alerts = relationship("Alert", back_populates="owner")

    __table_args__ = (UniqueConstraint("email", "tel", name="uq_email_tel"),)


class Alert(Base):
    """Modelo de Alert"""
    __tablename__ = "alerts"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    search_url = Column(String, index=True)
    created_at = Column(DateTime)
    due_date = Column(DateTime)
    is_active = Column(Boolean, default=True)
    notify_by = Column(String)
    owner_id = Column(Integer, ForeignKey("users.id"))

    owner = relationship("User", back_populates="alerts")


class Publication(Base):
    """Modelo de Publicação"""
    __tablename__ = "publications"

    id = Column(Integer, primary_key=True)
    title = Column(String)
    price = Column(Numeric)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    link = Column(String)
    address = Column(String)
    alert_id = Column(Integer, ForeignKey("alerts.id"))

    __table_args__ = (UniqueConstraint("alert_id", "link", name="uq_alertid_link"),)

    def __init__(self, title, price, link, address, created_at, alert_id) -> None:
        self.title = title
        self.price = price
        self.created_at = created_at
        self.updated_at = created_at
        self.link = link
        self.address = address
        self.alert_id = alert_id
