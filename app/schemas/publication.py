from pydantic import BaseModel


class PublicationBase(BaseModel):
    title: str
    price: str
    link: str
    address: str


class PublicationCreate(PublicationBase):
    pass


class Publication(PublicationBase):
    updated_at: str
