from typing import List

from pydantic import BaseModel

from .alert import Alert


class UserBase(BaseModel):
    email: str
    tel: str
    address: str


class UserCreate(UserBase):
    password: str


class UserUpdate(UserBase):
    id: int
    password: str
    is_active: bool


class User(UserBase):
    id: int
    is_active: bool
    alerts: List[Alert] = []

    class Config:
        orm_mode = True
