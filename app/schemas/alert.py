# Modelo de como as requisições e respostas para uma rota devem chegar e sair

from pydantic import BaseModel
from datetime import datetime, timedelta


class AlertBase(BaseModel):
    title: str
    search_url: str
    created_at: datetime = datetime.now()
    due_date: datetime = datetime.now() + timedelta(days=30)
    notify_by: str


class AlertCreate(AlertBase):
    pass


class Alert(AlertBase):
    id: int
    owner_id: int

    class Config:
        orm_mode = True
