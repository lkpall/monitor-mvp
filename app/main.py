"""
Rotas par CRUD de usuários e alertas
"""

from typing import List

from . import crud, models

from .database import SessionLocal, engine

from .schemas.alert import Alert, AlertCreate
from .schemas.user import User, UserCreate, UserUpdate

from fastapi import Depends, FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi_utils.tasks import repeat_every

from monitor.modules.browser import setup_driver
from monitor.pages.zapzap import ZapPage

from sqlalchemy.orm import Session


models.Base.metadata.create_all(bind=engine)

app = FastAPI()

app.driver = None

origins = [
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def get_db():
    """Dependency"""
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.on_event("startup")
def create_instance_driver() -> None:
    """
    Cria a instacia do Selenium na página do WhatsApp
    """
    driver = setup_driver(headless=False)
    driver.get("https://web.whatsapp.com")
    app.driver = ZapPage(driver)
    input("Escanei o QR Code e pressione Enter")


@app.on_event("shutdown")
def close_instance_driver() -> None:
    """
    Fecha a instãncia do Selenium
    """
    app.driver.driver.quit()


@app.on_event("startup")
@repeat_every(seconds=600, raise_exceptions=True)  # 10 min
def track_alerts() -> None:
    """
    Executa o monitoramento dos anúncios
    """
    with next(get_db()) as db:
        crud.track(app.driver, db)


@app.post("/users/", response_model=User)
def create_user(user: UserCreate, db: Session = Depends(get_db)):
    """Users POST"""
    db_user = crud.get_user_by_email_or_tel(db, email=user.email, tel=user.tel)

    if db_user:
        raise HTTPException(status_code=400, detail="Email or tel already registered")

    return crud.create_user(db=db, user=user)


@app.get("/users/", response_model=List[User])
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    """Users GET"""
    users = crud.get_users(db, skip=skip, limit=limit)

    return users


@app.get("/users/{user_id}", response_model=User)
def read_user(user_id: int, db: Session = Depends(get_db)):
    """User GET by ID"""
    db_user = crud.get_user(db, user_id=user_id)

    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    return db_user


@app.put("/users/", response_model=User)
def update_user(user: UserUpdate, db: Session = Depends(get_db)):
    """User PUT by ID"""
    db_user = crud.update_user(db, user)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@app.delete("/users/{user_id}")
def delete_user(user_id: int, db: Session = Depends(get_db)):
    """User DELETE by ID"""
    crud.delete_user(db=db, user_id=user_id)
    return {"ok": True}


@app.post("/users/{user_id}/alert/", response_model=Alert)
def create_alert_for_user(user_id: int, alert: AlertCreate, db: Session = Depends(get_db)):
    """
    User ALERT by User ID
    """
    return crud.create_user_alert(db=db, alert=alert, user_id=user_id)


@app.get("/alerts/", response_model=List[Alert])
def read_alerts(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    """
    USERS ALERTS GET
    """
    items = crud.get_alerts(db, skip=skip, limit=limit)
    return items


@app.delete("/alerts/{alert_id}/")
def delete_alert(alert_id: int, db: Session = Depends(get_db)):
    """
    ALERT DELETE
    """
    crud.delete_alert(db, alert_id)

    return {"ok": True}
