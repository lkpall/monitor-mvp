# Funções de que fazem o CRUD diretamente com o banco

from sqlalchemy import or_
from sqlalchemy.orm import Session

from typing import List

from .schemas.user import UserCreate, UserUpdate
from .schemas.alert import AlertCreate

from .models import User, Alert

from core import monitor_publication
from sqlalchemy.sql.expression import true

from monitor.pages.zapzap import ZapPage


def get_user(db: Session, user_id: int) -> User:
    """
    Busca o usuário com o id informado

    Params
    ------
    - db: sessão do banco
    - user_id: id do usuário

    Returns
    -------
    User: usuário encontrado
    """
    return db.query(User).filter(User.id == user_id).first()


def get_user_by_email_or_tel(db: Session, email: str, tel: str) -> User:
    """
    Busca o usuário pelo email ou telefone informado

    Params
    ------
    db: sessão do banco
    email: email do usuário
    tel: telefone do usuário

    Returns
    -------
    User: usuário encontrado
    """
    return db.query(User).filter(or_(User.email == email, User.tel == tel)).first()


def get_users(db: Session, skip: int = 0, limit: int = 100) -> List[User]:
    """
    Busca todos os usuários cadastrados

    Params
    ------
    - db: sessão do banco
    - skip: pesquisa a partir de um número
    - limit: limite de itens a serem retornados

    Returns
    -------
    - Lista de usuários cadastrados
    """
    return db.query(User).offset(skip).limit(limit).all()


def create_user(db: Session, user: UserCreate) -> User:
    """
    Cria o usuário no banco

    Params
    ------
    - db: sessão do banco
    - user: schema com campos necessarios para cadastrar o usuario

    Returns
    -------
    - User: usuario criado
    """
    fake_hashed_password = user.password + "notreallyhashed"
    db_user = User(
        email=user.email,
        hashed_password=fake_hashed_password,
        tel=user.tel,
        address=user.address
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    return db_user


def update_user(db: Session, user: UserUpdate) -> User:
    """
    Atualiza os dados do usuário

    Params
    ------
    - db: sessão do banco
    - user: schema com campos necessarios para atualizar o usuario

    Returns
    -------
    - User: usuario atualizado
    """
    db_user = get_user(db, user.id)

    for field, value in vars(user).items():
        setattr(db_user, field, value) if value else None

    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    return db_user


def delete_user(db: Session, user_id: int) -> None:
    """
    Deleta o usuário

    Params
    ------
    - db: sessão no banco
    - user_id: id do usuário
    """
    db_user = get_user(db, user_id)
    db.delete(db_user)
    db.commit()

    return


def create_user_alert(db: Session, alert: AlertCreate, user_id: int) -> Alert:
    """
    Cria o alerta dos anúncios

    Params
    ------
    - db: sessão do banco
    - alert: schema com campos necessarios para criar o alerta
    - user_id: id do usuário

    Returns
    -------
    - Alert: alerta criado
    """
    alert = alert.dict()
    alert['owner_id'] = user_id
    new_alert = Alert(**alert)
    db.add(new_alert)
    db.commit()
    db.refresh(new_alert)

    return new_alert


def get_alerts(db: Session, skip: int = 0, limit: int = 100) -> List[Alert]:
    """
    Pega todos os alertas criados

    Params
    ------
    - db: sessão do banco
    - skip: pesquisa a partir de um número
    - limit: limite de itens a serem retornados

    Returns
    -------
    - Lista de alertas cadastrados
    """
    return db.query(Alert).offset(skip).limit(limit).all()


def delete_alert(db: Session, alert_id: int) -> None:
    """
    Deleta o alerta informado

    Params
    ------
    - db: sessão do banco
    - alert_id: id do alerta
    """
    alert = db.query(Alert).filter(Alert.id == alert_id).first()
    db.delete(alert)
    db.commit()

    return


def track(driver: ZapPage, db: Session) -> None:
    """
    Monitora os alertas criados

    Params
    ------
    - driver: objeto da página do WhatsApp
    - db: sessão do banco
    """
    alerts = db.query(Alert).filter(Alert.is_active == true()).join(
        User, User.id == Alert.owner_id
    ).with_entities(
        Alert.id,
        Alert.search_url,
        Alert.title,
        Alert.notify_by,
        User.email,
        User.tel
    )

    for id, url, title, notify_by, email, tel, in alerts:
        notifier = {'email': email} if notify_by == "email" else {'tel': tel}

        monitor_publication(
            id=id,
            url=url,
            title=title,
            notifier=notifier,
            driver_zap=driver,
            db=db
        )

    return
